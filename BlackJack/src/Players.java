import java.util.LinkedList;
import java.util.List;

public class Players {
    private final String name;
    private final boolean isDealer;
    private double money;
    private List<LinkedList<Card>> hand = new LinkedList<>();
    private double bet;
    private boolean stand;
    private boolean surrender;
    private int currentHand; // for splits

    private int insuranceBet;

    public Players(String name, double money, boolean dealer) {
        this.name = name;
        this.isDealer = dealer;
        this.money = dealer ? 9999999999D : money;
    }

    public void gainCard(Card c, int handNum) {
        hand.get(handNum).add(c);
        checkBust();
    }

    /**
     * If the hand value > 21 the player busts.
     * Since the player busted and the card is reset, the hand < 2 indicated that the game reset
     * 
     * @return if the player busted
     */
    public boolean checkBust() {
        return getHandValue() > 21 || hand.size() < 2;
    }

    public boolean setBet(double d) {
        if (this.bet > money) {
            System.out.println(name + "'s bet is too much! " + bet + " " + money);
            return false;
        }
        this.bet = d;
        this.money -= bet;
        return true;
    }

    public void setStand(boolean b, boolean clear) {
        this.stand = b;
        if (!clear) {
            this.showHand(false);
            System.out.println(name + " is now standing.\n");
        }
    }

    public void doubleDown(Card c, int handNum) {
        this.gainCard(c, handNum);
        System.out.println("DOUBLE DOWN! " + name + "'s bet is now : " + (bet * 2));
        System.out.println(name + " has gained a " + c.getCardName());
        this.bet *= 2;
        setStand(true, false);
    }

    public void hit(Card c, int handNum) {
        gainCard(c, handNum);
        System.out.println(name + " has gained a " + c.getCardName());
    }

    public void surrender(int handNum) {
        this.bet /= 2;
        money += bet; // we need this line because we remove money in the beginning
        this.surrender = true;
        System.out.println(name + " surrendered! " + name + "'s bet is cut in half (" + this.bet + ").");
        System.out.println(name + " lost: " + this.bet + " making " + name + "'s total: " + this.money);
        this.stand = true;
    }

    public void split(int handNum) {

    }

    public void win(int dealerScore, boolean insurance) {
        if (insurance) {
            System.out.println(name + "'s insurance bet paid off!");
        } else if (dealerScore > 21) {
            System.out.println("Dealer bust!");
        } else {
            System.out.println(name + "'s score: " + this.getHandValue() + " beat the dealer's " + dealerScore);
        }

        double winnings;
        if (insurance) {
            winnings = insuranceBet * 2;
        } else if (getHandValue() == 21 && hand.size() == 2) {
            System.out.println("Blackjack! 2.5x win");
            winnings = bet * 2.5;
        } else {
            winnings = bet * 2;
        }
        money += winnings;
        System.out.printf("%s won %f making %s's total %f\n\n", name, winnings, name, money);
        reset();
    }

    public void win(int dealerScore) {
        win(dealerScore, false);
    }

    public void lose(int dealerScore) {
        if (checkBust()) {
            System.out.println(name + " bust!");
        } else {
            System.out.println(name + " score: " + this.getHandValue() + " lost to the dealer's " + dealerScore);
        }
        System.out.printf("%s lost %f making %s's total %f\n\n", name, bet, name, money);
        reset();
    }

    public void push() {
        System.out.println("Push! " + name + "'s score " + this.getHandValue() + " drew with the dealer.");
        double winnings = this.bet;
        money += winnings;
        System.out.println(name + " won: " + winnings + " making " + name + "'s total: " + money);
        reset();
    }

    private void reset() {
        // Clear cards for next round, reset game
        for (int i = 0; i < 4; i++) {
            getHand(0).clear();
        }
        this.insuranceBet = 0;
        setStand(false, true);
        setSurrender(false);
    }

    // UTILITY METHODS
    public void showHand(boolean reveal) {
        if (isDealer) {
            System.out.println("Dealer's Hand:");
            for (int i = 0; i < hand.size(); i++) {
                if (i == 1 && !reveal) {
                    System.out.print("?? ");
                } else {
                    System.out.print(hand.get(0).get(i).getCardName() + " ");
                }
            }
        } else {
            System.out.println(name + "'s hand:");
            for (LinkedList<Card> hands : hand) {
                for (Card card : hands) {
                    System.out.print(card.getCardName() + " ");
                }
            }
        }
        System.out.println();
    }

    public int getHandValue() {
        int value = 0;
        boolean hasAce = false;
        for (LinkedList<Card> hands : hand) {
            for (Card c : hands) {
                if (c.getName() == 'A') {
                    hasAce = true;
                    continue;
                }
                value += c.getValue();
            }
        }
        if (hasAce) {
            if (value > 11) {
                value++;
            } else {
                value += 11;
            }
        }
        return value;
    }

    public void setSurrender(boolean b) {
        this.surrender = b;
    }

    // GETTERS
    public String getName() {
        return name;
    }

    public double getBet() {
        return bet;
    }

    public double getMoney() {
        return money;
    }

    public boolean getStand() {
        return stand;
    }

    public List<Card> getHand(int handNum) {
        return hand.get(handNum);
    }

    public boolean isSurrender() {
        return surrender;
    }

    public int getCurrentHandNumber() {
        return currentHand;
    }

    public void setCurrentHandNumber(int x) {
        this.currentHand = x;
    }

    public int getInsuranceBet() {
        return insuranceBet;
    }

    public void setInsuranceBet(int x) {
        this.insuranceBet = x;
    }
}
