import java.util.LinkedList;

public class Deck {
    private LinkedList<Card> theDeck;
    private char[] suits = { 'S', 'H', 'C', 'D' };

    public Deck(int size) {
        if (size % suits.length != 0) {
            throw new RuntimeException("Must be mod " + suits.length);
        }
        if (size > 52) {
            throw new RuntimeException("Must be under 52");
        }

        theDeck = new LinkedList<Card>();
        for (int i = 1; i <= size / suits.length; i++) {
            for (char c : suits) {
                theDeck.add(new Card(c, getCardNameByIndex(i), getCardValueByIndex(i)));
            }
        }
    }

    public LinkedList<Card> getDeck() {
        return theDeck;
    }

    private static char getCardNameByIndex(int i) {
        switch (i) {
            case 1:
                return 'A';
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
                return (char) ('0' + i);
            case 10:
                return 'T';
            case 11:
                return 'J';
            case 12:
                return 'Q';
            case 13:
                return 'K';
            default:
                throw new RuntimeException("Should never get here");
        }
    }

    private static int getCardValueByIndex(int i) {
        switch (i) {
            case 1:
                return 11;
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
                return i;
            case 10:
            case 11:
            case 12:
            case 13:
                return 10;
            default:
                throw new RuntimeException("Should never get here");
        }
    }
}
